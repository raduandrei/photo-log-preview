# Photo-log
This is only a preview of one of my private projects. If you want to see the source code please ask me for project access.
An awesome photo gallery application written in Go!

![alt text](./docs/5.png "Edit Gallery Page 2")


## Short description
Photo-log is a website where photographers can create photo galleries to share
with their clients.  
Photographers can sign up, create galleries, and upload photos all from within
their dashboard.  
After a gallery is completed it can be published, making it publicly accessibly
to anyone with the URL and allowing photographers to share their work with their
clients.
  
For a complete list of all the subjects this projects covers please see [subjects](docs/subjects.md).

## Installing
Create the folder structure and git clone the project.
```bash
$ mkdir -p ~/go/src
$ cd ~/go/src
$ git clone git@gitlab.com:raduandrei/photo-log.git
```

Get all dependencies.
```bash
$ go get -t github.com/gorilla/csrf
$ go get -t github.com/gorilla/mux
$ go get -t github.com/gorilla/schema
$ go get -t github.com/jinzhu/gorm
$ go get -t github.com/jinzhu/gorm/dialects/postgres
$ go get -t golang.org/x/crypto/bcrypt
$ go get -t gopkg.in/mailgun/mailgun-go.v1
```

### Installing PostgreSQL
This guide is for Ubuntu 16.04. If you are running Ubuntu please install PostgreSQL as described in the following guide:
https://www.calhoun.io/how-to-install-postgresql-9-5-on-ubuntu-16-04/

Connect to psql with user_name `postgres` and password `postgres`.
```bash
psql -U <user_name> 
```

Create a database inside Postgres, in our case the `db_name` will be `photolog`.
```bash
CREATE DATABASE <db_name>;
```
Our App uses the following PostgreSQL Dev Database Server Connection Information:
```
username postgres  
password postgres  
db name is photolog
host localhost  
port 5432
```

## Running the application
Before running the application, the postgres server must be already running.
To check the status of the service we can do:
```bash
systemctl status postgresql.service
```
When running locally the environment variable in [.config](./.config) file
must be set to `dev` because of the CSRF implementation we use when running in `prod` mode.
Then simply run:
```bash
$ go run main.go config.go

Starting the server on :3000...Enjoy
```
## More documentation
- For a preview of how the Website UI looks like please see [preview](docs/preview.md).
- For more useful commands to interact with the DB please see [postgresql](docs/postgresql.md).

