### Useful psql commands to interact with the database
- Starting the PostgreSQL server and checking the service status
```bash
systemctl start postgresql.service  
systemctl status postgresql.service
```
- Connect to the Postgres server with user `postgres` and password `postgres` (When prompted for your password, type it in)
```bash
psql -U <username> 
```

- Create a database inside Postgres
```bash
CREATE DATABASE <db_name>;
```

- List all databases
```bash
\l
```

- Connect to a db inside psql
```bash
\c <db_name>
```

- List all tables
```bash
\dt
```

- Select all from table
```bash
SELECT * FROM <table_name>;
```

- Drop table
```bash
DROP TABLE <table_name>
```
